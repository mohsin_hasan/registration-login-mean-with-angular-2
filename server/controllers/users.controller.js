var config = require('../config');
var express = require('express');
const path = require('path');
var router = express.Router();

var userService = require('../services/user.service');

/**
 * mailjet email service provider
 */
const mailjet = require('node-mailjet').connect('b37148108c4f5c1f8c6a521d3ec4179f', '1dcd0b1875bf09bb648d71f149bc4862');

/**
 * SendGrid email service provider
 */
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/current', getCurrent);
router.put('/:_id', update);
router.delete('/:_id', _delete);
// router.post('/forget-password', forgetPassword);
// router.post('/reset-password', resetPassword);

module.exports = router;


function authenticate(req, res) {
	userService.authenticate(req.body.username, req.body.password)
		.then((user) => {
			if (user)
				res.send(user);
		}, (err) => {
			res.status(400).send(err);
		});
}

function register(req, res) {
	userService.create(req.body)
		.then(() => {
			res.sendStatus(200);
		}, (err) => {
			res.status(400).send(err);
		});
}

function getAll(req, res) {
	userService.getAll(req, res)
		.then((users) => {
			res.send(users);
		}, (err) => {
			res.status(400).send(err);
		})
}

function getCurrent(req, res) {
	userService.getById(req.user.sub)
		.then((user) => {
			if (user)
				res.send(user)
			else
				res.sendStatus(404);
		}, (err) => {
			res.status(400).send(err);
		});
}

function update(req, res) {
	userService.update(req.params._id, req.body)
		.then(() => {
			res.sendStatus(200);
		}, (err) => {
			res.status(400).send(err);
		})
}

function _delete(req, res) {
	userService.delete(req.params._id)
		.then(function () {
			res.sendStatus(200);
		})
		.catch(function (err) {
			res.status(400).send(err);
		});
}

/*function resetPassword(req, res) {
	userService.resetPassword(req.body.username)
		.then(function () {
			res.json({
				success: true,
				message: 'A link has been sent to your email address. Please check your email to reset passWord'
			});
		})
		.catch(function (err) {
			res.status(400).send(err);
		});
}*/

router.post('/forget-password', function (req, res, next) {
	userService.forgetPassword(req.body.username)
		.then(function (token) {
			res.token = token;
			next();
		})
		.catch(function (err) {
			res.status(400).send(err);
		});
}, function (req, res) {
	sendEmailBySendGrid(req, res, res.token);
});


router.post('/reset-password', function(req, res, next){
	console.log("req : ", req);

	userService.resetPassword(req.body, req.query.token)
	.then(function (data) {
		res.json({
			status : 200,
			message: data
		})
	})
	.catch(function (err) {
		res.status(400).send(err);
	});
});

/**
 * Method to test 'MailJet' integration with NodeJs to send emails to users.
 */
function sendEmailByMailJet() {
	var sendEmail = mailjet.post('send');

	var emailData = {
		'FromEmail': 'mohsin.hasan@daffodilsw.com',
		'FromName': 'Mohsin Hasan',
		'Subject': 'Test with the NodeJS Mailjet wrapper',
		'Text-part': 'Hello NodeJs (mailJet) testing!',
		'Recipients': [{ 'Email': 'mohsin.hasan.cs@gmail.com' }, { 'Email': 'mohsin.hasan@daffodilsw.com' }]
	}

	sendEmail
		.request(emailData)
		.then(handlePostResponse)
		.catch(handleError);
}

function handlePostResponse(res) {
	console.log("Mail response : ", res);
}

function handleError(err) {
	console.log("Mail error : ", err);
}

/**
 * Method to test 'SendGrid' integration with NodeJs to send emails to users.
 */
function sendEmailBySendGrid(req, res, token) {
	let username = req.body.username.split('.')[0];
	let resetPasswordUrl = 'http://localhost:4200/reset-password?token=' + token;

	const msg = {
		to: 'mohsin.hasan@daffodilsw.com',
		from: 'mohsin.hasan.cs@gmail.com',
		subject: 'Sending with SendGrid is Fun',
		text: 'and easy to do anywhere, even with Node.js',
		html: '<div>' +
			'<h3>Dear ' + `${username},` + '</h3>' +
			'<p>You requested for a password reset, kindly use this <a href="' + `${resetPasswordUrl}` + '">link</a> to reset your password</p>' +
			'<br>' +
			'<p>Cheers!</p>' +
			'</div>',
	};
	sgMail.send(msg)
		.then(function () {
			res.json({
				success: true,
				message: 'A link has been sent to your email address. Please check your email to reset password'
			});
		})
		.catch(handleError);
}

