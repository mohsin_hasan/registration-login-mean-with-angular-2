import { Component, OnInit } from '@angular/core';

import { User } from '../_models/user';
import { AuthenticationService} from '../_services/authentication.service';
import { AlertService } from '../_services/alert.service';

@Component({
	selector: 'app-forget-password',
	templateUrl: './forget-password.component.html',
	styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

	user: any = {};
	loading = false;
	isResetButtonDisabled = false;

	constructor(
		private authenticationService:AuthenticationService,
		private alertService:AlertService
	) { }

	ngOnInit() {
	}

	forgetPassword() {
		this.loading = true;
		this.isResetButtonDisabled = true;
		this.authenticationService.forgetPassword(this.user).subscribe(res => {
			if(res.status === 200){
				this.alertService.success(res.json().message);
				this.loading = false;
			}
		},error => {
			this.alertService.error(error);
		});
	}

}
