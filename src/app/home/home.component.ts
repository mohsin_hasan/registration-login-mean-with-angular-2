import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../_services/user.service';
import { User } from '../_models/user';

import { Subject } from 'rxjs/Subject';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	loggedInUser: User;
	users: User[] = [];

	public userToUpdate = new Subject<User>();

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private userService: UserService) {
		this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
	}

	ngOnInit() {
		this.loadAllUsers();
	}

	loadAllUsers() {
		this.userService.getAll().subscribe(users => this.users = users);
	}

	deleteUser(_id: string) {
		this.userService.delete(_id).subscribe(() => { this.loadAllUsers() });
	}

	updateUser(user: User) {
		// this.userService.update(user).subscribe((res) => {
		// 	console.log("update response: ", res)
		// });
		// this.userToUpdate.next(user);
		// this.router.navigate(['/update']);

		this.userService.setUserDataToUpdate(user);

		this.router.navigate(['/update']);
	}

	getUserDetails(user:User){
		this.userService.getById(user._id).subscribe((res) => {
			console.log("current User :",res);
		})
	}

}
