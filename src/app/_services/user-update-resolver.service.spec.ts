import { TestBed, inject } from '@angular/core/testing';

import { UserUpdateResolverService } from './user-update-resolver.service';

describe('UserUpdateResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserUpdateResolverService]
    });
  });

  it('should be created', inject([UserUpdateResolverService], (service: UserUpdateResolverService) => {
    expect(service).toBeTruthy();
  }));
});
