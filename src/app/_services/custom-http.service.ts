import { Injectable } from '@angular/core';
import { Http, Headers, ConnectionBackend, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response } from '@angular/http';

import { appConfig } from '../app.config';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { retry } from 'rxjs/operators/retry';
import { headersToString } from 'selenium-webdriver/http';
import { Jsonp } from '@angular/http/src/http';
import { windowWhen } from 'rxjs/operators/windowWhen';

@Injectable()
export class CustomHttpService extends Http {

	constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
		super(backend, defaultOptions);
	}

	get(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.get(appConfig.apiUrl + url, this.addJwt(options))
			.catch(this.handleError);
	}

	post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.post(appConfig.apiUrl + url, body, this.addJwt(options)).catch(this.handleError);
	}

	put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.put(appConfig.apiUrl + url, body, this.addJwt(options)).catch(this.handleError);
	}

	delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
		return super.delete(appConfig.apiUrl + url, this.addJwt(options))
			.catch(this.handleError);
	}

	private addJwt(options?: RequestOptionsArgs): RequestOptionsArgs {
		options = options || new RequestOptions;
		options.headers = options.headers || new Headers();

		let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
		if (loggedInUser && loggedInUser.token)
			options.headers.append('Authorization', 'Bearer ' + loggedInUser.token);

		return options;
	}

	handleError(error: any) {
		if (error.status === 401)
			window.location.href = '/login';
		return Observable.throw(error._body);
	}
}

export function customHttpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
	return new CustomHttpService(xhrBackend, requestOptions);
}

export let customHttpProvider = {
	provide: Http,
	useFactory: customHttpFactory,
	deps: [XHRBackend, RequestOptions]
};
