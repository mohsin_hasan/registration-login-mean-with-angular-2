import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

// import { Subject } from 'rxjs/Subject';

import { User } from '../_models/user';

@Injectable()
export class UserService {

	// public userToUpdate = new Subject<User>();

	userDetails: User;

	constructor(private http: Http,
		private route: ActivatedRoute,
		private router: Router
	) { }

	getAll() {
		return this.http.get('/users').map((res: Response) => {
			return res.json();
		});
	}

	getById(_id: string) {
		return this.http.get('/users/current').map((response: Response) => response.json());
	}

	create(user: User) {
		return this.http.post('/users/register', user);
	}

	update(user: User) {
		// this.userToUpdate.next(user);
		// this.router.navigate(['/update']);
		return this.http.put('/users/' + user._id, user);
		// return null;
	}

	delete(_id: string) {
		return this.http.delete('/users/' + _id);
	}

	getUserToUpdate() {
		return this.userDetails;
	}

	setUserDataToUpdate(user: User) {
		this.userDetails = user;
	}
}
