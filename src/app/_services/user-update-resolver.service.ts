import { Injectable, ViewChild } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'

import { Observable } from 'rxjs/Observable';

import { User } from '../_models/user';
import { UserService } from './user.service';

@Injectable()
export class UserUpdateResolverService implements Resolve<any> {

	userDetails: User;

	constructor(
		private router: Router,
		private userService: UserService
	) {
		// this.userService.userToUpdate.subscribe((response) => {
		// 	console.log("Response : ", response);
		// 	this.userDetails = response;
		// });

		// this.home.userToUpdate.subscribe((response) => {
		// 	console.log("Response : ", response);
		// 	this.userDetails = response;
		// });

		
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		// this.userService.userToUpdate.subscribe((response) => {
		// 	console.log("Response : ", response);
		// 	this.userDetails = response;
		this.userDetails = this.userService.getUserToUpdate();
		
		if (this.userDetails)
			return this.userDetails;
		return null;
		// });

	}

}
