import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationEnd } from '@angular/router';

import { AuthenticationService } from '../_services/authentication.service';
import { AlertService } from '../_services/alert.service';

import * as $ from 'jquery';

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
	user: any = {};
	token: any;
	message:string;
	loading = false;
	isResetButtonDisabled = false;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		private alertService: AlertService
	) {
		router.events.subscribe(event => {
			if (event instanceof NavigationEnd && event.url.includes("/reset-password")) {
				this.route.queryParams.subscribe(param => {
					if (!param.token)
						this.router.navigate(['/forget-password']);
					else
						this.token = param.token;
				});
			}
		});
	}

	ngOnInit() { }

	resetPassword() {
		this.loading = true;
		this.isResetButtonDisabled = true;

		if(this.token){
			this.authenticationService.resetPassword(this.user, this.token).subscribe(res => {
				if(res.status === 200){
					this.message = res.json().message;
					this.alertService.success('');
					this.loading = false;
					let url = '/login';
					let alertMessage = this.message;
					setTimeout(function(){
						$("div.alert-success").html(alertMessage + ' Now you can <a href="'+ url +'">login</a> with your new password');
					},300);					
				}
			}, error => {
				this.alertService.error(error);
			});
		}
		
	}

}
