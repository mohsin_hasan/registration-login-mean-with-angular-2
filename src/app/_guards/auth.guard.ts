import { Injectable } from '@angular/core';
import { Router, NavigationStart, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private router: Router) { }
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): any {

		// this.router.events.subscribe(event => {
		// 	// if (event instanceof NavigationStart) {
		// 	// 	console.log("navigation start")
		// 	// 	// if (localStorage.getItem('loggedInUser'))
		// 	// 	// 	return true;
		// 	// 	// this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
		// 	// 	// return false;
		// 	// }
		// 	// NavigationEnd
		// 	// NavigationCancel
		// 	// NavigationError
		// 	// RoutesRecognized
		// 	console.log("event : ", event);
		// });

		if (localStorage.getItem('loggedInUser'))
			return true;
		this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
		return false;
	}
}
